package com.humanbooster.businesscase.servlets;


import com.humanbooster.businesscase.models.Candidat;
import com.humanbooster.businesscase.services.CandidatService;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CandidatAddServlet extends HttpServlet {

    private CandidatService candidatService;

    public CandidatAddServlet() {
        super();
        this.candidatService = new CandidatService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/candidat-add.jsp").forward(request, response);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<String> errorForm = new ArrayList<String>();

        String nom = request.getParameter("nom");
        String prenom = request.getParameter("prenom");
        Date dateNaissance = null;
        try {
            dateNaissance = new SimpleDateFormat("yyyy-MM-dd")
                    .parse(request.getParameter("dateNaissance"));
        } catch (ParseException e) {
            errorForm.add("Format de la date incorrecte !");
        }
        String adresse = request.getParameter("adresse");
        String codePostal = request.getParameter("codePostal");
        String ville = request.getParameter("ville");

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Candidat candidat = new Candidat(nom, prenom, dateNaissance, adresse, ville, codePostal);

        Set<ConstraintViolation<Candidat>> errors = validator.validate(candidat);


        if (errors.size() == 0) {

            this.candidatService.create(candidat);
            response.sendRedirect("/candidats");
        } else {
            request.setAttribute("errors", errors);
            this.doGet(request, response);
        }


    }
}
