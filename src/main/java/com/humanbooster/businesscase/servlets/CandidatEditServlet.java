package com.humanbooster.businesscase.servlets;


import com.humanbooster.businesscase.models.Candidat;
import com.humanbooster.businesscase.services.CandidatService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class CandidatEditServlet extends HttpServlet {

    private CandidatService candidatService;

    public CandidatEditServlet() {
        super();
        this.candidatService = new CandidatService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        Candidat candidat = null;
        Integer id = Integer.parseInt(request.getParameter("id"));


            candidat = this.candidatService.getById(id);

        if (candidat == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        } else {
            request.setAttribute("candidat", candidat);
            request.getRequestDispatcher("/candidat-edit.jsp").forward(request, response);
        }


    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Candidat candidat = null;
        Integer id = Integer.parseInt(request.getParameter("id"));


            candidat = this.candidatService.getById(id);

        if (candidat == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        } else {
            request.setAttribute("candidat", candidat);


            List<String> errorForm = new ArrayList<String>();

            String nom = request.getParameter("nom");

            if (nom.isEmpty()) {
                errorForm.add("Veuillez saisir un nom !");
            }

            String prenom = request.getParameter("prenom");

            if (prenom.isEmpty()) {
                errorForm.add("Veuillez saisir un prénom !");
            }

            Date dateNaissance = null;
            try {
                dateNaissance = new SimpleDateFormat("yyyy-MM-dd")
                        .parse(request.getParameter("dateNaissance"));
            } catch (ParseException e) {
                errorForm.add("Format de la date incorrecte !");
            }

            String adresse = request.getParameter("adresse");
            if (adresse.isEmpty()) {
                errorForm.add("Veuillez saisir une adresse !");
            }


            String codePostal = request.getParameter("codePostal");

            if (codePostal.isEmpty()) {
                errorForm.add("Veuillez saisir un code postal !");
            }

            String ville = request.getParameter("ville");

            if (codePostal.isEmpty()) {
                errorForm.add("Veuillez saisir une ville !");
            }


            if (errorForm.size() == 0) {

                candidat.setNom(nom);
                candidat.setPrenom(prenom);
                candidat.setAdresse(adresse);
                candidat.setCodePostal(codePostal);
                candidat.setDateNaissance(dateNaissance);
                candidat.setVille(ville);


                    this.candidatService.update(candidat);


                response.sendRedirect("/candidats");
            } else {
                request.setAttribute("errors", errorForm);
                this.doGet(request, response);
            }

        }


    }
}
