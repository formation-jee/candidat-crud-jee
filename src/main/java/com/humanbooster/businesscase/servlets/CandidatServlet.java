package com.humanbooster.businesscase.servlets;

import com.humanbooster.businesscase.models.Candidat;
import com.humanbooster.businesscase.services.CandidatService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CandidatServlet extends HttpServlet {

    private CandidatService candidatService;

    public CandidatServlet() {
        super();
        this.candidatService = new CandidatService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Candidat> candidats =new ArrayList<>();


            candidats = this.candidatService.getAll();


        request.setAttribute("candidats", candidats);

        request.getRequestDispatcher("candidat-list.jsp").forward(request, response);
    }




}
