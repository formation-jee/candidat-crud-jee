package com.humanbooster.businesscase.servlets;

import com.humanbooster.businesscase.models.Candidat;
import com.humanbooster.businesscase.services.CandidatService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CandidatDetailServlet extends HttpServlet {
    private CandidatService candidatService;

    public CandidatDetailServlet() {
        super();
        this.candidatService = new CandidatService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Candidat candidat = null;
        Integer id = Integer.parseInt(request.getParameter("id"));


            candidat = this.candidatService.getById(id);


        if (candidat == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        } else {
            request.setAttribute("candidat", candidat);

            request.getRequestDispatcher("candidat-detail.jsp").forward(request, response);
        }
    }
}
