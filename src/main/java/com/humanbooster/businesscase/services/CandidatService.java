package com.humanbooster.businesscase.services;

import com.humanbooster.businesscase.models.Candidat;
import com.humanbooster.businesscase.utils.DAO;

import com.humanbooster.businesscase.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.Date;
import java.util.List;

public class CandidatService implements DAO<Candidat, Integer> {

    private SessionFactory sessionFactory;

    public CandidatService() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    @Override
    public List getAll() {

        Session session = this.sessionFactory.getCurrentSession();

        session.beginTransaction();

        String hql = "FROM Candidat candidat";

        Query query = session.createQuery(hql);

        List candidats = query.getResultList();

        session.close();
        return candidats;
    }


    @Override
    public Candidat getById(Integer id)  {

        Session session = this.sessionFactory.getCurrentSession();

        session.beginTransaction();
        Candidat candidat = session.get(Candidat.class, id);

        session.close();
        return candidat;
    }

    @Override
    public Candidat create(Candidat candidat) {
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(candidat);
        session.flush();
        session.close();

        return candidat;
    }

    @Override
    public Candidat update(Candidat candidat) {
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.merge(candidat);

        session.flush();
        session.close();
        return candidat;
    }

    @Override
    public void delete(Candidat candidat) {
        Session session = this.sessionFactory.getCurrentSession();

        session.beginTransaction();
        session.delete(candidat);

        session.flush();
        session.close();
    }
}
