package com.humanbooster.businesscase.utils;

import java.sql.SQLException;
import java.util.List;

public interface DAO<T, ID> {
    List<T> getAll() ;
    T getById(ID id) ;
    T create(T t) ;
    T update(T t);
    void delete(T t) ;
}
