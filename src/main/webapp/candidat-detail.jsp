<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!doctype html>
<html lang="fr">
<head>
    <%@ include file='/parts/stylesheets.jsp' %>
</head>
<body>

<%@ include file='parts/header.jsp' %>

<div id="global">
    <h1>Fiche détaillée du candidat ${candidat.prenom} - ${candidat.nom}</h1>

    <div class="text-center">
        <i><span>Nom</span></i> : ${candidat.nom} <br>
        <i><span>Prenom</span></i> : ${candidat.prenom} <br>
        <i><span>Date de naissance</span></i> : <fmt:formatDate pattern="dd/MM/yyyy" value="${candidat.dateNaissance}"/> <br>
        <i><span>Adresse : </span></i> : ${candidat.adresse} ${candidat.codePostal} ${candidat.ville}
    </div>

    <div class="d-flex justify-content-center m-10">
        <a class="primaryBtn mt-10 mb-10" href="/candidats">Retour</a>
    </div>

</div>
</body>
</html>