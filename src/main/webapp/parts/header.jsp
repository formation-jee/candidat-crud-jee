<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<header id="header">
    <div class="d-flex justify-content-center m-10">
        <a class="primaryBtn mt-10 mb-10" href="/candidats">Home</a>
    </div>

    <div class="d-flex justify-content-center m-10">
        <a class="primaryBtn mt-10 mb-10" href="/candidats/add">Ajouter un candidat</a>
    </div>
</header>