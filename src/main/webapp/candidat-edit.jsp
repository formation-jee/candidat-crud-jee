<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!doctype html>
<html lang="fr">
<head>
    <%@ include file='/parts/stylesheets.jsp' %>
</head>
<body>

<%@ include file='parts/header.jsp' %>

<div id="global">
    <form method="post">
        <div class="form-group">
            <label for="nom">Nom</label>
            <input type="text" class="form-control" value="${candidat.nom}" id="nom" name="nom" placeholder="Entrez le nom du candidat">
        </div>


        <div class="form-group">
            <label for="nom">Prénom</label>
            <input type="text" class="form-control" id="prenom" value="${candidat.prenom}" name="prenom" placeholder="Entrez le prénom du candidat">
        </div>

        <div class="form-group">
            <label for="nom">Date de naissance</label>
            <input type="date" name="dateNaissance" value="${candidat.dateNaissance}" class="form-control" id="dateNaissance" placeholder="dd/mm/yyyy">
        </div>


        <div class="form-group">
            <label for="nom">Adresse</label>
            <input type="text" name="adresse" value="${candidat.adresse}" class="form-control" id="adresse" placeholder="Veuillez saisir une adresse">
        </div>

        <div class="form-group">
            <label for="nom">Ville</label>
            <input type="text" class="form-control" value="${candidat.ville}" name="ville" id="ville" placeholder="Veuillez saisir une ville">
        </div>

        <div class="form-group">
            <label for="nom">Code postal</label>
           <%-- <input type="text" class="form-control" value="${candidat.codePostal}" id="cp" name="codePostal" placeholder="Veuillez saisir un code postal">
--%>
            <input type="radio" name="codePostal" <c:if test="${candidat.codePostal == 63190}">checked</c:if> value="63190"> 63190
            <input type="radio" name="codePostal" <c:if test="${candidat.codePostal == 63000}">checked</c:if> value="63000"> 63000
            <input type="radio" name="codePostal" <c:if test="${candidat.codePostal == 63130}">checked</c:if> value="63130"> 63130

        </div>

        <div class="col-md-12 text-center">
            <c:forEach items="${errors}" var="error">

                <span class="error">Erreur : <c:out value="${error}"/></span><br>

            </c:forEach>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>


    </form>

</div>
</body>
</html>