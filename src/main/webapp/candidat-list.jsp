<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8"/>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <title>Gestion des candidats</title>
</head>
<body>

<%@ include file='/parts/header.jsp' %>

<div id="global" class="container">
    <div class="row">

        <c:forEach var="candidat" items="${candidats}">
            <div class="col-md-6 m-10">
            <div class="card" style="width: 18rem; margin-top:10px;">
                <div class="card-body">
                    <h5 class="card-title text-center">${candidat.nom} - ${candidat.prenom}</h5>
                    <div class="col-md-12 text-center">
                        <a href="/candidats/edit?id=${candidat.id}" class="btn btn-warning mb-2">Editer !</a><br>
                        <a href="/candidats/delete?id=${candidat.id}" class="btn btn-danger mb-2">Supprimer !</a><br>
                        <a href="/candidat-detail?id=${candidat.id}" class="btn btn-primary mb-2">Voir le détail !</a>
                    </div>
                </div>
            </div>
            </div>
        </c:forEach>

    </div>

    <hr/>
</div>
</body>
</html>